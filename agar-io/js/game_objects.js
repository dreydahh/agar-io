//Types definition
//****************
function CircleObject (x, y, r, c, n)
{
	this.x = x;
	this.y = y;
	this.radius = r;
	this.color = c;
	this.name = n;
	this.isEnableDrawing = true;
	this.setPosition = function(x, y)
	{
		this.x = x;
		this.y = y;
	};
	this.translate = function(x, y)
	{
		this.x += x;
		this.y += y;
	};
	this.isOverlaping = function(circleObject)
	{
		if(this.radius > circleObject.radius)
		{
			var distance = Math.sqrt(Math.pow(this.x - circleObject.x, 2) + Math.pow(this.y - circleObject.y, 2));
			return distance < (this.radius - circleObject.radius)
		}
		else
		{
			return false;
		}
	};
	this.draw = function()
	{
		if(this.isEnableDrawing)
		{
			//Drawing circle
			context.beginPath();
			context.arc(this.x, this.y, this.radius, 0, 2*Math.PI);
			context.fillStyle = this.color;
			context.fill();
			context.closePath();
			
			//Drawing name text
			if(this.name != "")
			{
				context.font = "20px Arial";
				context.strokeStyle = "black";
				context.textAlign = "center";
				context.fillText(this.name, this.x, this.y - this.radius - 10);

			}
		}
	};
}
//****************

//Game objects definition
//****************
function Player(id, name, x, y, radius)
{
	this.id = id;
	this.body = new CircleObject(x, y, radius, "#2196F3", name);
	this.speed = 5;
	this.minSpeed = 2;

	//viewport.setCenterPosition(x, y);
}

function Enemy(id, x, y, radius, name)
{
	this.id = id;
	this.body = new CircleObject(x, y, radius, "#F44336", name);
}

function Food(id, x, y, r)
{
	this.id = id;
	this.body = new CircleObject(x, y, r, "#FFFF00", "");
	this.value = 2;
}
//****************