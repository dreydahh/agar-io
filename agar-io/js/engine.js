//Play / Pause game
//****************
var isGamePlaying = true;
function playGame()
{
	isGamePlaying = true;
}

function pauseGame()
{
	isGamePlaying = false;
}
//****************

//Input
//****************
var inputKeys = [];
const LEFT_ARROW = 37;
const UP_ARROW = 38;
const RIGHT_ARROW = 39;
const DOWN_ARROW = 40;
window.addEventListener("keydown", function(e)
	{
		inputKeys[e.keyCode] = true;
	}, false);

window.addEventListener("keyup", function(e)
	{
		delete inputKeys[e.keyCode];
	}, false);
function isKeyDown(keyCode)
{
	return inputKeys[keyCode] == true;
}
//****************

//Engine core loop
//****************
var engineInterval;
function startEngine()
{
	clearInterval(engineInterval);
	engineInterval = setInterval(function()
	{
		if(isGamePlaying)
		{
			getMapRequest();
			//update();
			//render();
		}
	}, 1000/FPS);
}
//****************

function getFrame()
{
	if(isGamePlaying)
	{
		update();
		render();
	}
}