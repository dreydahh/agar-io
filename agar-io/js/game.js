const FPS = 40;
const MAP_CLEAR_COLOR = "black";
const MAP_BG_COLOR = "#212121"
const FOOD_MIN_NUMBER = 25;
const FOOD_MAX_NUMBER = 50;
const GAME_OVER_SCREEN_OUT_TIME = 2500;

var player;
var enemy = new Array();
var foodObjects;

function reset()
{
	viewport.reset();
}

function update()
{
	moveInput();
}

function render()
{
	viewport.clear();
	drawFood();
	player.body.draw();
	for(var i = 0; i < enemy.length; i++)
	{
		enemy[i].body.draw();
	}

}

//Overlaping check
//****************
function checkIfPlayerOverlapFood()
{
	for(var i = 0; i < foodObjects.length; i++)
	{
		if(foodObjects[i].body.isEnableDrawing && player.body.isOverlaping(foodObjects[i].body))
		{
			foodObjects[i].body.isEnableDrawing = false;
			player.body.radius += foodObjects[i].value * 2;
			if(player.speed > player.minSpeed)
			{
				player.speed -= (foodObjects[i].value / 10);
			}
			else
			{
				player.speed = player.minSpeed;
			}
		}
	}
}

function checkIfPlayerOverlapEnemy()
{
	if(enemy.body.isEnableDrawing && player.body.isOverlaping(enemy.body))
	{
		enemy.body.isEnableDrawing = false;
		gameOverWin(player.body.name);
	}
}
//****************

function generateFood(foodDics)
{
	foodObjects = [];
	for(var i = 0; i < foodDics.length; i++)
	{
		var foodDic  = foodDics[i];
		foodObjects[i] = new Food(foodDic["id"], foodDic["x"], foodDic["y"], foodDic["radius"]);
	}
}

function drawFood()
{
	for(var i = 0; i < foodObjects.length; i++)
	{
		foodObjects[i].body.draw();
	}
}

function playerMove()
{
	//checking input
	if(isKeyDown(RIGHT_ARROW))
	{
		player.body.translate(player.speed, 0);
	}
	else if(isKeyDown(LEFT_ARROW))
	{
		player.body.translate(-player.speed, 0);
	}
	if(isKeyDown(UP_ARROW))
	{
		player.body.translate(0, player.speed);
	}
	else if(isKeyDown(DOWN_ARROW))
	{
		player.body.translate(0, -player.speed);
	}

	//constraints
	if(player.body.x < 0)
	{
		player.body.x = 0;
	}
	else if(player.body.x > canvas.width)
	{
		player.body.x = canvas.width;	
	}
	if(player.body.y < 0)
	{
		player.body.y = 0;
	}
	else if(player.body.y > canvas.height)
	{
		player.body.y = canvas.height;	
	}
}

function moveInput()
{
	if(isKeyDown(LEFT_ARROW))
	{
		moveRequest(player.speed, 0);
	}
	else if(isKeyDown(RIGHT_ARROW))
	{
		moveRequest(-player.speed, 0);
	}
	if(isKeyDown(DOWN_ARROW))
	{
		moveRequest(0, -player.speed);
	}
	else if(isKeyDown(UP_ARROW))
	{
		moveRequest(0, player.speed);
	}
}

function gameOver(killerId)
{
	var killerName = "";
	var message;
	if(killerId > -1)
	{
		for(var i=0; i < enemy.length; i++)
		{
			if(enemy[i].id == killerId)
			{
				killerName = enemy[i].body.name;
				break;
			}
		}
		message = "[ " + killerName + " ] ate you like a snack!";
	}
	else
	{
		message = "you get killed!";
	}

	pauseGame();
	drawGameOverScreen(message);
	
	setTimeout(function()
		{
			reset();
			playGame();
			setName();
		}, GAME_OVER_SCREEN_OUT_TIME);
}

function drawGameOverScreen(message)
{
	context.fillStyle = MAP_BG_COLOR;
	context.fillRect(viewport.getOriginX(), viewport.getOrignY(), viewport.width + canvas.width, viewport.height + canvas.height);
	context.font = "45px Arial";
	context.fillStyle = "#F44336";
	context.textAlign = "center";
	context.fillText(message, viewport.getCenterX(), viewport.getCenterY());
}