package pl.agarclone.game;

public class GameObject {
	public static int lastID = 0;
	protected int id;
	protected float x;
	protected float y;
	protected float radius;
	
	public GameObject(float x, float y, float radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		
		// set next available ID
		id = GameObject.lastID;
		GameObject.lastID++;
	}
	
	public int getID() {
		return id;
	}
	public void setID(int id) {
		this.id = id;
	}
	public float getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public float getRadius() {
		return radius;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
}
