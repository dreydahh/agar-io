# README #
# agar-io game clone made for study project on Wroclaw University of Technology

## REQUIRED SOFTWARE
> - [Apache Tomcat](http://tomcat.apache.org/download-90.cgi)

## INSTALLATION
> 1. Clone repository or unpack zip file.
> 2. Copy **agar-io** directory and **AgarClone.war** to **webapps** located in apachtomcat main dircetory.
> 3. Open **agar-io/js/rest_sservice.js** and assign your **server address** to **SERVER** const value.
> 4. Save and close file.
> 5. Server side application works on port 8080.

## STARTUP
> 1. Run **startup script** from **bin** in apachtomcat main dircetory.
> 2. Open web browser on client and go to **[server_addres]:8080/agar-io** or **[your_hostname]/agar-io**.

## USAGE
> - On main page there is text field for user name and button to begin game.
> - To controll character in game use: **ArrowUp, ArrowDown, ArrowRight, ArrowLeft**
> - Collect yellow **food object** to increase your character size.
> - Try to eat smaller opponents.
> - Watch out for bigger opponents.